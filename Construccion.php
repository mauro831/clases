<?php
/**
 * Descripcion de la clase  Construccion
 *
 * @author MAURO
 */
class Construccion 
{
    
    private $TipoZona;
    private $Ancho;
    private $Largo;
    private $MetrosCuadrados;
    private $Area;
    private $AreaConstruccion;
    
    public function __construct()
    {
        
        $this->TipoZona="";
        $this->Ancho=0;
        $this->Largo=0;
        $this->MetrosCuadrados=0;
        $this->Area=0;
        $this->AreaConstruccion=0;
        
    }
    
    public function __destruct() 
    {
        
    }
    
    public function getTipoZona() 
    {
        return $this->TipoZona;
    }
    
    public function getAncho() 
    {
        return $this->Ancho;
    }

    public function getLargo() 
    {
        return $this->Largo;
    }

    public function getMetrosCuadrados() 
    {
        return $this->MetrosCuadrados;
    }

    public function setTipoZona($TipoZona) 
    {
        $this->TipoZona = $TipoZona;
    }
    
    public function setAncho($Ancho) 
    {
        $this->Ancho = $Ancho;
    }

    public function setLargo($Largo) 
    {
        $this->Largo = $Largo;
    }

    public function setMetrosCuadrados($MetrosCuadrados) 
    {
        $this->MetrosCuadrados = $MetrosCuadrados;
    }
    function getArea() {
        return $this->Area;
    }

    function getAreaConstruccion() {
        return $this->AreaConstruccion;
    }

    function setArea($Area) {
        $this->Area = $Area;
    }

    function setAreaConstruccion($AreaConstruccion) {
        $this->AreaConstruccion = $AreaConstruccion;
    }

     public function AreaConstruccion()
    {
        $this->Area=$this->Largo*$this->Ancho;     
    }
}
