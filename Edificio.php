<?php

/**
 * Description of Edificio
 *
 * @author MAURO
 */
include'./Construccion';
class Edificio extends Construccion
{
    private $NroAlcobas;
    private $NroPiso;
    private $NroApartamentos;
    private $TipoConstruccion;
    private $Ubicacion;
    
    public function __construct()
    {
        parent::__construct();
        $this->NroAlcobas="";
        $this->NroPisos="";
        $this->NroApartamentos="";
        $this->TipoConstruccion="";
        $this->Ubicacion="";

        
    }
    
     public function __destruct() 
    {
        parent::__destruct();
        echo "<br>Hasta pronto";
    }
   
    public function getNroAlcobas() 
    {
        return $this->NroAlcobas;
    }

    public function getNroPiso() 
    {
        return $this->NroPiso;
    }

    public function getNroApartamentos() 
    {
        return $this->NroApartamentos;
    }

    public function setNroAlcobas($NroAlcobas) 
    {
        $this->NroAlcobas = $NroAlcobas;
    }

    public function setNroPiso($NroPiso) 
    {
        $this->NroPiso = $NroPiso;
    }

    public function setNroApartamentos($NroApartamentos) 
    {
        $this->NroApartamentos = $NroApartamentos;
    }
    
    public function setTipoConstruccion($TipoConstruccion) 
    {
        $this->TipoConstruccion = $TipoConstruccion;
    }
    
    public function getTipoConstruccion() 
    {
        return $this->TipoConstruccion;
    }
    public function getUbicacion() 
    {
        return $this->Ubicacion;
    }

    public function setUbicacion($Ubicacion) 
    {
        $this->Ubicacion = $Ubicacion;
    }
    
    public function TipoConstruccion($TipoConstruccion)
    {
        switch($TipoConstruccion){
            case "Casa":
                 $this->TipoConstruccion = "Casa";
                 break;
             case "Edificio":
                 $this->TipoConstruccion = "Edificio";
                 break;
        }
    }
    public function Ubicacion($Ubicacion)
    {
       switch($Ubicacion){
           case "Urbana":
                $this->Ubicacion = "Urbana";
                break;
            case "Rural":
                $this->Ubicacion = "Rural";
                break;
       }
   }

}
