<?php
/**
 * Description of Casa
 *
 * @author MAURO
 */
include'./Construccion.php';
class Casa extends Construccion
{
    private $NroAlcobas;
    private $NroPatios;
    private $NroJardines;
    private $TipoConstruccion;
    private $Ubicacion;
    
    public function __construct()
    {
        parent::__construct();
        $this->NroAlcobas="";
        $this->NroPatios="";
        $this->NroJardines="";
        $this->TipoConstruccion="";
        $this->Ubicacion="";
    }
    
    public function __destruct() 
    {
        parent::__destruct();
        echo "<br>Hasta pronto";
    }
    
    public function getNroAlcobas()
    {
        return $this->NroAlcobas;
    }

    public function getNroPatios() 
    {
        return $this->NroPatios;
    }

    public function getNroJardines() 
    {
        return $this->NroJardines;
    }

    public function setNroAlcobas($NroAlcobas) 
    {
        $this->NroAlcobas = $NroAlcobas;
    }

    public function setNroPatios($NroPatios) 
    {
        $this->NroPatios = $NroPatios;
    }

    public function setNroJardines($NroJardines) 
    {
        $this->NroJardines = $NroJardines;
    }
    
    public function setTipoConstruccion($TipoConstruccion) 
    {
        $this->TipoConstruccion = $TipoConstruccion;
    }
    
    public function getTipoConstruccion() 
    {
        return $this->TipoConstruccion;
    }
    public function getUbicacion() 
    {
        return $this->Ubicacion;
    }

    public function setUbicacion($Ubicacion) 
    {
        $this->Ubicacion = $Ubicacion;
    }
    
    public function TipoConstruccion($TipoConstruccion)
    {
        switch($TipoConstruccion){
            case "Casa":
                $this->TipoConstruccion = "Casa";
                 break;
             case "Edificio":
                 $this->TipoConstruccion = "Edificio";
                 break;
        }
    }
    public function Ubicacion($Ubicacion)
    {
       switch($Ubicacion){
           case "Urbana":
                $this->Ubicacion = "Urbana";
                break;
            case "Rural":
                $this->Ubicacion = "Rural";
                break;
       }
   }
 
}
